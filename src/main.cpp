/*
licence  : GPL v.3 et CC BY 3.0 FR, plus dinfo sur : https://www.gnu.org/licenses/quick-guide-gplv3 et https://creativecommons.org/licenses/by/3.0/fr/
Réalisateur du projet   : Ethan BEAUVAIS-GUIBERT et Adrien
notre site web : https://ospcar.github.io/
Nous contacter : https://ospcar.github.io/contact.html
Tous les codes et schémas : https://ospcar.github.io/code.html
Suivre le projet : https://ospcar.github.io/suividirect.html

Comment ouvrire les fichiers Arduino (atom): https://projetsdiy.fr/bien-demarrer-platformio-ide-arduino-esp8266-esp32-stm32/
*/

// Programme de la radio-commande

#include <Arduino.h>
#include "RF24.h"
#include "printf.h"
#include "nRF24L01.h"

#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif
U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);

#define ANGLE_SERVO_MOTOR 1    // for the debuging// for the debuging // Key words enter by the users (Serial)
#define POTAR_DIRECTION_ADJUSTEMENT 2
#define POTAR_SPEED_ADJUSTEMENT 3
#define POTAR_DIRECTION 4
#define POTAR_SPEED 5
#define SPEED 6
#define PING 7
#define WIFI_CONFIG 8
#define ALL 9
#define HELP 10
#define STOP 11
#define NOT_FOUND 12

// Déclaration des fonctions
int direction ();
int vitesse ();
int string_compare (char input_words[20]);


const int potar_vitesse = A2; // Définition du potentiomètre de potar_vitesse
const int potar_ajustement_vitesse = A3;

const int potar_direction = A0; // Définition potentiomètre de direction
const int potar_ajustement_direction = A1; // Définition potentiomètre d'ajustement de direction

RF24 radio_commande(7,8); //CE,CSN  Pin du module radio (NRF24L01 +)
const byte address_envoie [6] = "00001"; //Adress de la radio_commande pour l'Envoie
const byte address_reception [6] = "00010";

const byte led_red = 2;

unsigned char pong_failed_count = 0;

struct data_Package // Max 32 bytes NRF24L01 buffer limit
{
  unsigned int direction;
  int vitesse;
  bool ping = false;
  bool pong = false;
  byte gps_speed;//Speed by the gps module
  byte battery_level_car = 0;
};
data_Package data;

void setup ()
{
  printf_begin();
  pinMode (potar_direction, INPUT); // Déclaration en sortie des potentiomètres //Broche A0
  pinMode (potar_ajustement_direction, INPUT); //Broche A1

  analogReference (EXTERNAL); //Définition d'une meilleure qualité sur les potentiomètres sur la broche "AREF" de l'Arduino.
  Serial.begin (9600); //Initialisation de la voie série

  radio_commande.begin();
  radio_commande.openWritingPipe(address_envoie);
  radio_commande.openReadingPipe(0, address_reception);
  radio_commande.setPALevel(RF24_PA_MIN);
  radio_commande.stopListening();


  u8g2.begin();// Setup the screen : SSD1306
}
bool etat_ecran = 0; // 0 -> Boot screen; 1 -> home screen
byte load = 0; //Bar Prgresse on Boot screen
bool show;
unsigned long flash_battery = 0, time = 0, time1 = 0, time2 = 0;
char message_voie_serie [20]; //tableau contenant les messages envoyé via la voie série par l'utilisateur
bool connected; // If the car is connectes to the radio.

void loop ()
{
  switch (etat_ecran) //Display the differents screens
  {
    case 0://boot screen
    {
      do {

        u8g2.setFont(u8g2_font_koleeko_tf);
        u8g2.drawStr((127 - u8g2.getStrWidth("O.S.PCar"))/2,63,"O.S.PCar");
        u8g2.drawRFrame((127-100)/2,15,100,22,7);
        u8g2.drawRBox(16,18.5,load + 15,15,7);

      } while ( u8g2.nextPage() );
      u8g2.firstPage();
      delay (4);
      load ++;
      if (load > 78)
      {
        etat_ecran = 1;
        delay (2000);
        u8g2.clearDisplay ();
      }
      break;
    }

    case 1:
    {
      do
      {
        if (connected)
        {
          u8g2.setFont(u8g2_font_open_iconic_other_2x_t); //The car is connected
          u8g2.drawGlyph(0, 16, 0x46);
        }
        else
        {
          u8g2.setFont(u8g2_font_open_iconic_www_2x_t);//The car is deconnected
          u8g2.drawGlyph(0, 16, 0x45);
        }

        switch (data.battery_level_car)
        {
          case 0: //Battery is dead
          {
            if (millis() - flash_battery > 1000) //Flash
            {
              flash_battery = millis ();
              bool etat = show;
              show =! etat;
            }
            if (show)
            {
              u8g2.setFont(u8g2_font_battery19_tn);
              u8g2.drawGlyph(5, 60,48);
              if (!connected)
              {
                u8g2.setFont (u8g2_font_pxplusibmcgathin_8u);
                u8g2.drawStr(13, 60, "?");
              }
            }
            break;
          }

          case 1:
          {
            u8g2.setFont(u8g2_font_battery19_tn);
            u8g2.drawGlyph(5, 60,49);
            break;
          }

          case 2:
          {
            u8g2.setFont(u8g2_font_battery19_tn);
            u8g2.drawGlyph(5, 60,50);
            break;
          }

          case 3:
          {
            u8g2.setFont(u8g2_font_battery19_tn);
            u8g2.drawGlyph(5, 60,51);
            break;
          }

          case 4:
          {
            u8g2.setFont(u8g2_font_battery19_tn);
            u8g2.drawGlyph(5, 60,52);
            break;
          }

          case 5://Battery is fully charged
          {
            u8g2.setFont(u8g2_font_battery19_tn);
            u8g2.drawGlyph(5, 60,53);
            break;
          }
        }

        byte y = map (data.vitesse,0,255,58,21);// Position y of gas bar
        u8g2.drawFrame(30,20,7,40);//gas bar
        u8g2.drawLine(29, y, 37, y);//Cusror
        u8g2.setFont(u8g2_font_open_iconic_all_1x_t);//gaz Symbol
        u8g2.drawGlyph(20, 28,245);

        y = map (analogRead (potar_ajustement_direction),0,1023, 21,58);// Position y of way bar
        u8g2.drawFrame(42,20,7,40);// way bar
        u8g2.drawLine(41,y,49, y);//Cusrsor
        u8g2.setFont(u8g2_font_open_iconic_all_1x_t);//direction Symbol
        u8g2.drawGlyph(51, 28,246);

        u8g2.setFont (u8g2_font_7Segments_26x42_mn);//Show the speed
        u8g2.setCursor(65, 55);
        u8g2.print(data.gps_speed);

        u8g2.setFont (u8g2_font_lucasfont_alternate_tf);
        u8g2.setCursor(100, 63);
        u8g2.print("km/h");


      } while (u8g2.nextPage());
      u8g2.firstPage();
      break;
    }
  }
  data.vitesse = vitesse();
  data.direction = direction();

  radio_commande.write (&data, sizeof (data_Package)); //Send data to the car

  if ((data.ping == true) && (millis() - time2 > 500)) //Send a message to check if a RC is connected but the car return also the battery level and the speed
  {
    delay (10);
    radio_commande.startListening();
    time1 = millis();
    while (millis() - time1 < 50)
    {
      if (radio_commande.available())
      radio_commande.read (&data, sizeof (data_Package));
    }

    if (data.pong)
    {
      connected = true;
      pong_failed_count = 0;
    }
    else if (pong_failed_count > 3)
    {
      data.battery_level_car = 0;
      pong_failed_count = 0;
      connected = false;
    }
    else
    pong_failed_count ++;

    data.ping = false;
    data.pong = false;
    radio_commande.stopListening();
    time2 = millis();
  }

  else if (millis() - time2 > 1000)
  data.ping = true;

  if (millis() - time > 500)
  {
    byte i = 0;
    if (Serial.available() > 0)
    {
      while (Serial.available() > 0)
      {
        message_voie_serie [i] = Serial.read (); //Enregistrement des données dans le tableau 'message_voie_serie'
        i++;
        delay (5); // laisse un peu de temps entre chaque accès à la mémoire
      }
      i -= 2;
      message_voie_serie [i] = '\0';
    }
    time = millis ();
  }

  char message [200] = "";
  if (!strcmp (message_voie_serie,"") == 0)
  {
    int mot = string_compare (message_voie_serie);

    if (mot == ANGLE_SERVO_MOTOR)
    sprintf(message,"Servomotor : %d°", direction()); // Assemble le texte avec les variables

    else if (mot == POTAR_DIRECTION_ADJUSTEMENT)
    sprintf(message,"Potar direction adj. : %d",analogRead (potar_ajustement_direction));

    else if (mot == POTAR_SPEED_ADJUSTEMENT)
    sprintf(message,"Potar speed adj. : %d",analogRead (potar_ajustement_vitesse));

    else if (mot == POTAR_DIRECTION)
    sprintf(message,"Potar direction : %d",analogRead(potar_direction));

    else if (mot == POTAR_SPEED)
    sprintf(message,"Potar speed: %d",analogRead (potar_vitesse));

    else if (mot == SPEED)
    sprintf(message,"Speed: %d",vitesse());

    else if (mot == PING)
    {
      printf ("Envoie d'une requette à l'adresse suivante : %s",address_envoie);
      data.ping = true;
      message_voie_serie [0] = '\0';
    }

    else if (mot == ALL)
    sprintf(message,"Servomotor : %d°;  Potar direction : %d;   Potar direction adj : %d;   Potar speed adj : %d;  Potar speed : %d;    Speed: %d; connected : %d",direction(),analogRead(potar_direction),analogRead (potar_ajustement_direction),analogRead (potar_ajustement_vitesse),analogRead (potar_vitesse),vitesse(),connected);

    else if (mot == HELP)
    {
      Serial.println (F(""));
      Serial.println (F("V 0.1"));
      Serial.println (F(""));
      Serial.println (F("licence  : GPL v.3 et CC BY 3.0 FR, plus dinfo sur : https://www.gnu.org/licenses/quick-guide-gplv3 et https://creativecommons.org/licenses/by/3.0/fr/"));
      Serial.println (F("Réalisateur du projet   : Ethan BEAUVAIS-GUIBERT et Adrien"));
      Serial.println (F("notre site web : https://ospcar.github.io/"));
      Serial.println (F("Nous contacter : https://ospcar.github.io/contact.html"));
      Serial.println (F("Tous les codes et schémas : https://ospcar.github.io/code.html"));
      Serial.println (F("Suivre le projet : https://ospcar.github.io/suividirect.html"));
      Serial.println (F("Comment ouvrire les fichiers Arduino : https://projetsdiy.fr/bien-demarrer-platformio-ide-arduino-esp8266-esp32-stm32/"));
      Serial.println (F(""));
      Serial.println (F("'angle-servo'      | Afficher l'angle du servomoteur"));
      Serial.println (F("'potDirAd'         | Afficher valeur du potentiomètre d'ajustement"));
      Serial.println (F("'potDirection'     | Afficher valeur du potentiomètre de direction"));
      Serial.println (F("'potSpd'           | Afficher valeur du potentiomètre de vitesse"));
      Serial.println (F("'all'              | Affiche tout"));
      Serial.println (F("'ping'             | Envoie d'un ping à la radio-commande"));
      Serial.println (F("'wifi-config'      | Envoie la configuration wifi de la radio-commande"));
      Serial.println (F("'stop'             | stop le défilement"));
      Serial.println (F("'help'             | Afficher l'aide"));
      message_voie_serie [0] = '\0';
    }

    else if (mot == NOT_FOUND)
    {
      sprintf(message,"%s not found enter 'help' for more informations",message_voie_serie);
      message_voie_serie[0] ='\0';
    }

    else if (mot == STOP)
    message_voie_serie[0] ='\0';

    Serial.println (message); //envoie du texte sur la voie série
  }
}


/***************************************************************************
* Function Name: vitesse
* Description:  définit la vitesse en fonction des potentiomètres
* Parameters:
* Return: la vitesse entre -255 et +255
***************************************************************************/
int vitesse ()
{
  int vitesse = 0;
  if (!(analogRead(potar_vitesse) > 450 && analogRead(potar_vitesse) < 490))  // Valeur relaché du potentiomètre "> 450" "< 480" Supprime les parasites
  {
    if (analogRead(potar_vitesse) >= 490)
    {
      float gas = map(analogRead (potar_ajustement_vitesse),0, 1023,10, 5); //gas
      vitesse = map (analogRead(potar_vitesse),480,740,150,255 * gas); //convertion sur 5V
    }
    else
    {
      float gas = map(analogRead (potar_ajustement_vitesse),0, 1023,-10, -5); //gas
      vitesse = map (analogRead(potar_vitesse),220, 450,255 * gas, 0); //convertion sur 5V
    }
  }
  return min(255, vitesse/10);
}


/***************************************************************************
* Function Name: direction
* Description:  calcul l'angle du servo moteur
* Parameters:
* Return: l'angle du servo
***************************************************************************/
int angle_servo;
int direction () //Fonction de calcul de l'angle que prend le servomoteur en fonction des deux potentiomètres celui de direction et d'ajustement
{
  //déclaration de variable locale
  int ajustementDirection;
  ajustementDirection = map (analogRead (potar_ajustement_direction),0,1023,75,98); // convertion de la valeur reçu du potentiomètre d'ajustement de direction (valeur d'entrée MIN-MAX 0-1023) sur une plage comprise entre 75 et 98° soit une correction de +- 11.5° pour chaque coté
  angle_servo = map (analogRead (potar_direction),33,940,ajustementDirection + 45,ajustementDirection - 45); //Envoie de l'angle au servomoteur en fonction du potentiomètre de direction et celui de correction
  return angle_servo;
}


/***************************************************************************
* Function Name: string_compare
* Description:  compare les mots de la voie série à des mot clé
* Parameters: input_words, mots venant de la voie série
* Return: yes
***************************************************************************/
int string_compare (char input_words[20])
{
  if (strcmp (input_words, "angle-servo") == 0) //compare deux chaines de caractères, si il y a correspondence la fonction renvoie 0
  return ANGLE_SERVO_MOTOR;

  else if (strcmp (input_words, "potDirAd") == 0)
  return POTAR_DIRECTION_ADJUSTEMENT;

  else if (strcmp (input_words, "potSpdAd") == 0)
  return POTAR_SPEED_ADJUSTEMENT;

  else if (strcmp (input_words, "potDirection") == 0)
  return POTAR_DIRECTION;

  else if (strcmp (input_words, "potSpd") == 0)
  return POTAR_SPEED;

  else if (strcmp (input_words, "ping") == 0)
  return PING;

  else if (strcmp (input_words, "wifi-config") == 0)
  return WIFI_CONFIG;

  else if (strcmp (input_words, "speed") == 0)
  return SPEED;

  else if (strcmp (input_words, "all") == 0)
  return ALL;

  else if (strcmp (input_words, "help") == 0)
  return HELP;

  else if (strcmp (input_words, "stop") == 0)
  return STOP;

  else
  return NOT_FOUND;
}
